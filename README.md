# Mur des secrets

## Description

Ce projet contient le code d'une application faite avec [Framework7](hhttps://framework7.io).
Cette application permet de créer et consulter des murs de secrets. Pour les consulter il y a deux règles :

1. Si nous voulez lire un secret vous devez d'abord en déposer
   un.
2. Le secret doit vous concerner.

 Si vous ne respectez pas ces règles, vous aurez des problèmes.

## Installation du projet 

La première étape, installation du projet :

```
git clone https://gitlab.com/lpbb/mur-des-secrets
```

### Sur un server

Pour lancer cette application sur un server il suffit de rentrer les commandes :

```
cd frontend
npm install
npm start
```
L'application sera ouverte sur `http://localhost:3000`.

### Sur un android

Avec l'aide d'android studio vous pouvez lancer les commandes :
```
cd frontend
npm run build
npx cap add android
npx cap open android 
```

## Authors

Louis Perraud.
Oriane Foussard.
