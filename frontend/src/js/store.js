import { createStore } from "framework7/lite";

const store = createStore({
  state: {
    loading: false,
    idmurs: 0,
    idsecrets: 0,
    murs: [],
    // murs: [
    //   {
    //     id: 0,
    //     title: "mur test",
    //     listSecrets: [{ id: 0, text: "je suis un secret" }],
    //   },
    // ],
    idcurrentMur: 0,
    authorized: false,
    dark: false,
  },

  getters: {
    murs({ state }) {
      return JSON.parse(localStorage.getItem("murs"));
    },
    loading({ state }) {
      return state.loading;
    },
    idmurs({ state }) {
      return parseInt(localStorage.getItem("idmurs"));
    },
    idsecrets({ state }) {
      return parseInt(localStorage.getItem("idsecrets"));
    },
    idcurrentMur({ state }) {
      return state.idcurrentMur;
    },
    authorized({ state }) {
      return state.authorized;
    },
    dark({ state }) {
      return state.dark;
    },
  },

  actions: {
    addMur({ state }, mur) {
      let idmurs = parseInt(localStorage.getItem("idmurs"));
      let murs = JSON.parse(localStorage.getItem("murs"));
      localStorage.setItem("murs", JSON.stringify([...murs, mur]));
      localStorage.setItem("idmurs", idmurs + 1);
      state.murs = [...murs, mur];
      state.idmurs = idmurs + 1;
    },
    changeCurrentMur({ state }, id) {
      state.idcurrentMur = id;
    },
    addSecret({ state }, secret) {
      let murs = JSON.parse(localStorage.getItem("murs"));
      let idsecrets = parseInt(localStorage.getItem("idsecrets"));
      murs[state.idcurrentMur].listSecrets = [
        ...murs[state.idcurrentMur].listSecrets,
        secret,
      ];
      state.murs[state.idcurrentMur].listSecrets = [
        ...murs[state.idcurrentMur].listSecrets,
        secret,
      ];
      state.idsecrets = idsecrets + 1;
      localStorage.setItem("murs", JSON.stringify(murs));
      localStorage.setItem("idsecrets", idsecrets + 1);
    },
    Athorize({ state }) {
      state.authorized = true;
    },
    NotAthorize({ state }) {
      state.authorized = false;
    },
    ChangeMode({ state }) {
      state.dark = !state.dark;
    },
  },
});
export default store;
