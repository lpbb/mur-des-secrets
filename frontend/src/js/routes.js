import ReglesPage from "../pages/regles.jsx";
import AccueilPage from "../pages/accueil";
import ChoixMurPage from "../pages/choixMur.jsx";
import CreerMurPage from "../pages/creerMur.jsx";

import DynamicRoutePage from "../pages/dynamic-route.jsx";
import PoseSecretPage from "../pages/poseSecret.jsx";
import ConsulterSecretPage from "../pages/consulterSecret.jsx";

var routes = [
  {
    path: "/regles/",
    component: ReglesPage,
  },
  {
    path: "/choixMur/",
    component: ChoixMurPage,
  },
  {
    path: "/creerMur/",
    component: CreerMurPage,
  },
  {
    path: "/posesecret/",
    component: PoseSecretPage,
  },{
    path: "/consultersecret/",
    component: ConsulterSecretPage,
  },
  {
    path: "/accueil/",
    component: AccueilPage,
  },

  {
    path: "/dynamic-route/blog/:blogId/post/:postId/",
    component: DynamicRoutePage,
  },
];

export default routes;
