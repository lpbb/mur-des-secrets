import React, { useState, useEffect } from "react";
import { getDevice } from "framework7/lite-bundle";
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  Page,
  Navbar,
  Block,
  Button,
  Row,
  Segmented,
} from "framework7-react";

import capacitorApp from "../js/capacitor-app";
import routes from "../js/routes";
import store from "../js/store";

const MyApp = () => {
  // Login screen demo data
  const device = getDevice();
  // Framework7 Parameters
  const f7params = {
    name: "Mur des secrets ", // App name
    theme: "auto", // Automatic theme detection

    id: "io.framework7.myapp", // App bundle ID
    // App store
    store: store,
    // App routes
    routes: routes,
    // Register service worker (only on production build)
    serviceWorker:
      process.env.NODE_ENV === "production"
        ? {
            path: "/service-worker.js",
          }
        : {},
    // Input settings
    input: {
      scrollIntoViewOnFocus: device.capacitor,
      scrollIntoViewCentered: device.capacitor,
    },
    // Capacitor Statusbar settings
    statusbar: {
      iosOverlaysWebView: true,
      androidOverlaysWebView: false,
    },
  };
  f7ready(() => {
    // Init capacitor APIs (see capacitor-app.js)
    if (f7.device.capacitor) {
      capacitorApp.init(f7);
    }
    // Call F7 APIs here
  });
  if (localStorage.getItem("murs") == null) {
    localStorage.setItem("murs", JSON.stringify([]));
    localStorage.setItem("idmurs", 0);
    localStorage.setItem("idsecrets", 0);
  }
  // const isdark = () => {
  //   console.log(useStore("dark"))
  //   return useStore("dark");
  // };

  return (
    <App {...f7params}>
      {/* Left panel with cover effect*/}
      <Panel left reveal dark colorTheme="orange">
        <Page>
          <Navbar title="Acceuil" />
          <Block>
            <Row tag="p">
              <Button className="col" outline href="/choixMur/">
                Voir un mur
              </Button>
            </Row>
            <Row tag="p">
              <Button className="col" outline href="/creerMur/">
                Créer un mur
              </Button>
            </Row>
          </Block>
          <Segmented strong tag="p">
            <Button
              // active
              onClick={() => {
                store.dispatch("ChangeMode");
              }}
            >
              Dark Mode
            </Button>
          </Segmented>
        </Page>
      </Panel>

      {/* Right panel with reveal effect*/}
      <Panel right reveal dark>
        <View>
          <Page>
            <Navbar title="Règles" />
            <Block>
              <p>
                1. Si nous voulez lire un secret vous devez d'abord en déposer
                un.
              </p>
              <p>2. Le secret doit vous concerner.</p>
              <p>
                Si vous ne respectez pas ces règles, vous aurez des problèmes.
              </p>
            </Block>
          </Page>
        </View>
      </Panel>

      {/* Views/Tabs container */}
      <Views tabs className="safe-areas">
        <View id="view-regles" main tab tabActive url="/regles/" />
      </Views>
    </App>
  );
};
export default MyApp;
