import React, { useState, useEffect, useRef } from "react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  ListInput,
  Row,
  Col,
  Button,
  Popup,
  f7,
} from "framework7-react";
import store from "../js/store";
import { useStore } from "framework7-react";

const CreerMurPage = () => {
  const [titre, setTitre] = useState("");
  const murs = useStore("murs");
  const idmurs = useStore("idmurs");
  store.dispatch("NotAthorize");

  function handleClick(titre, idmur) {
    store.dispatch("addMur", { id: idmur, title: titre, listSecrets: [] });
    store.dispatch("changeCurrentMur", idmur);
    return;
  }
  const [popupOpened, setPopupOpened] = useState(false);
  const popup = useRef(null);

  const isdark = useStore("dark");
  return (
    <Page colorTheme="orange" dark={isdark}>
      {/* Top Navbar */}
      <Navbar large sliding={false}>
      <NavLeft>
          <Link
            iconIos="f7:home"
            iconAurora="f7:home"
            iconMd="material:home"
            panelOpen="left"
          />
        </NavLeft>
        <NavRight>
          <Link
            iconIos="f7:book"
            iconAurora="f7:book"
            iconMd="material:book"
            panelOpen="right"
          />
        </NavRight>
        <NavTitleLarge>Mur des secrets </NavTitleLarge>
      </Navbar>

      <Block strong>
        <List noHairlinesMd>
          <ListInput
            label="Titre"
            type="text"
            placeholder="Tritre de votre mur"
            clearButton
            onChange={(event) => setTitre(event.target.value)}
          ></ListInput>
          <Button
            className="col"
            outline
            onClick={() => handleClick(titre, idmurs)}
            popupOpen=".demo-popup"
            href="/posesecret/"
          >
            Créer
          </Button>
        </List>
      </Block>
      <Popup
        className="demo-popup"
        opened={popupOpened}
        onPopupClosed={() => setPopupOpened(false)}
        colorTheme="orange"
      >
        <Page>
          <Navbar title="Attention"></Navbar>
          <Block>
            <p>
              Lorsque vous créer un mur, vous devez déposer le premier secret.
            </p>
            <Link text="Poser un secret" popupClose />
          </Block>
        </Page>
      </Popup>
    </Page>
  );
};
export default CreerMurPage;
