import React from "react";
import { useStore } from "framework7-react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button,
} from "framework7-react";
import store from "../js/store";
import { useEffect, useState } from "react";
import { is } from "dom7";

const ChoixMurPage = () => {
  const murs = useStore("murs");
  store.dispatch("NotAthorize");

  function handleClick(idmur) {
    store.dispatch("changeCurrentMur", idmur);
    return;
  }
  const isdark = useStore("dark");
  return (
    <Page colorTheme="orange" dark={isdark}>
      <Navbar large sliding={false}>
        <NavLeft>
          <Link
            iconIos="f7:home"
            iconAurora="f7:home"
            iconMd="material:home"
            panelOpen="left"
          />
        </NavLeft>
        <NavRight>
          <Link
            iconIos="f7:book"
            iconAurora="f7:book"
            iconMd="material:book"
            panelOpen="right"
          />
        </NavRight>
        <NavTitleLarge>Mur des secrets </NavTitleLarge>
      </Navbar>
      <Block strong>
        {(murs.length !=0) && (
          <List title="Liste des murs">
            {murs.map((mur) => (
              <Button
                className="col"
                raised
                onClick={() => handleClick(mur.id)}
                href="/consultersecret/"
              >
                {mur.title}
              </Button>
            ))}
          </List>
        )}
        {(murs.length ==0) && (
          <Button className="col" raised href="/creerMur/">
            Créer un mur
          </Button>
        )}
      </Block>
    </Page>
  );
};

export default ChoixMurPage;
