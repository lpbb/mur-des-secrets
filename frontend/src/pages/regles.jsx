import React from "react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button,
} from "framework7-react";
import store from "../js/store";
import { useStore } from "framework7-react";

const ReglesPage = () => {
  const isdark = useStore("dark");
  return (
    <Page colorTheme="orange" dark={isdark}>
      {/* Top Navbar */}
      <Navbar large sliding={false}>
        <NavLeft>
          <Link
            iconIos="f7:home"
            iconAurora="f7:home"
            iconMd="material:home"
            panelOpen="left"
          />
        </NavLeft>
        <NavRight>
          <Link
            iconIos="f7:book"
            iconAurora="f7:book"
            iconMd="material:book"
            panelOpen="right"
          />
        </NavRight>
        <NavTitleLarge>Mur des secrets </NavTitleLarge>
      </Navbar>

      {/* Page content */}
      <Block strong>
        <h3>Règles du jeu</h3>
        <p>
          1. Si nous voulez lire un secret vous devez d'abord en déposer un.
        </p>
        <p>2. Le secret doit vous concerner.</p>
        <p>Si vous ne respectez pas ces règles, vous aurez des problèmes.</p>
      </Block>
      <Button className="col" href="/accueil/">
        Yes, compris
      </Button>
    </Page>
  );
};
export default ReglesPage;
