import React, { useState, useEffect, useRef } from "react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  ListInput,
  Row,
  Col,
  Button,
  Popup,
  f7,
} from "framework7-react";
import store from "../js/store";
import { useStore } from "framework7-react";

const PoseSecretPage = () => {
  const [texte, setTexte] = useState("");
  const idsecrets = useStore("idsecrets");
  const authorized = useStore("authorized");

  function handleClick(texte, idsecret) {
    store.dispatch("addSecret", { id: idsecret, text: texte });
    store.dispatch("Athorize");
    f7.dialog.alert(`Votre secret a bien été posé.`, () => {
      f7.loginScreen.close();
    });
  }

  const isdark = useStore("dark");
  return (
    <Page colorTheme="orange" dark={isdark}>
      {/* Top Navbar */}
      <Navbar large sliding={false}>
        <NavLeft>
          <Link
            iconIos="f7:home"
            iconAurora="f7:home"
            iconMd="material:home"
            panelOpen="left"
          />
        </NavLeft>
        <NavRight>
          <Link
            iconIos="f7:book"
            iconAurora="f7:book"
            iconMd="material:book"
            panelOpen="right"
          />
        </NavRight>
        <NavTitleLarge>Mur des secrets </NavTitleLarge>
      </Navbar>

      <Block strong>
        <List noHairlinesMd>
          <ListInput
            label="Contenu du Secret"
            type="textarea"
            placeholder="Votre secret"
            onChange={(event) => setTexte(event.target.value)}
          ></ListInput>
          <Button
            className="col"
            outline
            onClick={() => handleClick(texte, idsecrets)}
            href="/consultersecret/"
          >
            Poser
          </Button>
        </List>
      </Block>
    </Page>
  );
};
export default PoseSecretPage;
