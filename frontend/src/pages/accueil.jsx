import React from "react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button,
} from "framework7-react";
import store from "../js/store";
import { useStore } from "framework7-react";


const AccueilPage = () => {
  const isdark = useStore("dark");
  return (
    <Page colorTheme="orange" dark={isdark}>
    {/* Top Navbar */}
    <Navbar large sliding={false}>
      <NavLeft>
          <Link
            iconIos="f7:home"
            iconAurora="f7:home"
            iconMd="material:home"
            panelOpen="left"
          />
        </NavLeft>
        <NavRight>
          <Link
            iconIos="f7:book"
            iconAurora="f7:book"
            iconMd="material:book"
            panelOpen="right"
          />
        </NavRight>
        <NavTitleLarge>Mur des secrets </NavTitleLarge>
      </Navbar>

    <Block strong>
    <Row tag="p">
      <Button className="col" outline href="/choixMur/">
        Voir un mur
      </Button>
      </Row>
      <Row tag="p">
      <Button className="col" outline href="/creerMur/">
        Créer un mur
      </Button>
      </Row>
    </Block>
  </Page>
);}
export default AccueilPage;
