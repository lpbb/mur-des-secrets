import React from "react";
import { useStore } from "framework7-react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button,
  Popup,
} from "framework7-react";
import store from "../js/store";
import { useEffect, useState, useRef } from "react";

const ConsulterSecretPage = () => {
  const [currentSecret, setcurrentSecret] = useState({});
  const murs = useStore("murs");
  const idcurrentMur = useStore("idcurrentMur");
  const authorized = useStore("authorized");

  function handleClick(x) {
    store.dispatch("NotAthorize");
    setcurrentSecret(x);
    return;
  }
  function handleClick2() {
    store.dispatch("NotAthorize");
    return;
  }

  const [popupOpened, setPopupOpened] = useState(false);
  const popup = useRef(null);

  const isdark = useStore("dark");
  return (
    <Page colorTheme="orange" dark={isdark}>
      <Navbar large sliding={false}>
        <NavLeft>
          <Link
            iconIos="f7:home"
            iconAurora="f7:home"
            iconMd="material:home"
            panelOpen="left"
          />
        </NavLeft>
        <NavRight>
          <Link
            iconIos="f7:book"
            iconAurora="f7:book"
            iconMd="material:book"
            panelOpen="right"
          />
        </NavRight>
        <NavTitleLarge> Mur des secrets </NavTitleLarge>
      </Navbar>
      <Block>
        <BlockTitle> <h3>Mur des secrets de {murs[idcurrentMur].title}</h3></BlockTitle>
        <List>
          {!authorized && (
            <List title="Liste des Secrets">
              {murs[idcurrentMur].listSecrets.sort(()=> Math.random() - 0.5).map((secret) => (
                <Button
                  className="col"
                  raised
                  onClick={() => handleClick2(secret)}
                  popupOpen=".creersecretavant"
                  href="/posesecret/"
                >
                  Lire ce Secret
                </Button>
              ))}
            </List>
          )}
          {authorized && (
            <List title="Liste des Secrets">
              {murs[idcurrentMur].listSecrets.map((secret) => (
                <Button
                  className="col"
                  raised
                  onClick={() => handleClick(secret)}
                  popupOpen=".vuesecret"
                  href="/consultersecret/"
                >
                  Lire ce Secret
                </Button>
              ))}
            </List>
          )}
          <Button className="col" outline href="/posesecret/">
            Poser un secret
          </Button>
        </List>
      </Block>
      <Popup
        className="creersecretavant"
        opened={popupOpened}
        onPopupClosed={() => setPopupOpened(false)}
        colorTheme="orange"
      >
        <Page>
          <Navbar title="Attention"></Navbar>
          <Block>
            <p>Avant de consulter un secret, il faut en déposer un.</p>
            <Link text="Poser un secret" popupClose />
          </Block>
        </Page>
      </Popup>
      <Popup
        className="vuesecret"
        opened={popupOpened}
        onPopupClosed={() => setPopupOpened(false)}
        colorTheme="orange"
      >
        <Page>
          <Navbar title="GARDEZ LE SECRET"></Navbar>
          <Block>
            <p>
              {currentSecret.text}
              {console.log(currentSecret.text)}
            </p>
            <Link text="Close" popupClose />
          </Block>
        </Page>
      </Popup>
    </Page>
  );
};

export default ConsulterSecretPage;
